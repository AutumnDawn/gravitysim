#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include "include.h"
#include "game.h"

class Game;

namespace InputHandler
{
	std::vector<std::string> takeInput();
	void formatInput(std::string raw_input, std::vector<std::string> *input);
	void parseInput(const std::vector<std::string>& input, Game& game);
};

namespace Command
{
	void init(std::vector<std::string> args, Game& game);
	void run(std::vector<std::string> args, Game& game);
	void create(std::vector<std::string> args, Game& game);
	void falloff(Game& game);
	void collision_mode(std::vector<std::string> args, Game& game);
	void info(Game& game);
	void meta();
	void help();
};

#endif
