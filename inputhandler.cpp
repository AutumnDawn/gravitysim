// Made by, and copyright Autumn Dawn (2021-2023); license TBD.
#include "inputhandler.h"
#include "constants.h"

const char *GAMEINFO = "Made by, and copyright Autumn Dawn (2021-2023); license TBD.\nUses SDL2 for GUI.\n";

std::vector<std::string> InputHandler::takeInput()
{
	std::string raw_input;
	std::vector<std::string> input;
	
	std::getline(std::cin, raw_input);
	formatInput(raw_input, &input);
	return input;
}

void InputHandler::formatInput(std::string raw_input, std::vector<std::string> *input)
{
	unsigned long space = 0;
	input->clear();
	while (space != std::string::npos && space < raw_input.size()) {
		std::string c = raw_input.substr(space, raw_input.find(" ", space)-space);
		if (c != " ") {
			input->push_back(c);
		}
		space = raw_input.find(" ", space) + 1;
		if (space == 0)		// Because this idiotic piece of trash keeps breaking
			break;
	}
}

void InputHandler::parseInput(const std::vector<std::string>& input, Game& game)
{
	std::string com = input[0];
	if (com == "exit" || com == "quit" || com == "e" || com == "q")
		game.quit();
	else if (com == "init") {
		Command::init(input, game);
	} else if (com == "run") {
		Command::run(input, game);
	} else if (com == "create") {
		Command::create(input, game);
	} else if (com == "falloff") {
		Command::falloff(game);
	} else if (com == "collision_mode") {
		Command::collision_mode(input, game);
	} else if (com == "info") {
		Command::info(game);
	} else if (com == "meta") {
		Command::meta();
	} else if (com == "help") {
		Command::help();
	} else {
		std::cout << "Command not recongized.\n";
	}
}

void Command::init(std::vector<std::string> args, Game& game)
{
	if (args.size() == 1) {
		std::cout << "Usage: init [amount of bodies (0 or less to clear)] [x range] [z range] (default - 800:500)\n";
		return;
	} else {
		long bcount = atoi(args[1].c_str());
		long xrange = Constants::DEFAULT_WINDOW_XSIZE;
		long zrange = Constants::DEFAULT_WINDOW_RATIO*Constants::DEFAULT_WINDOW_XSIZE;
		game.clear_bodies();
		if (bcount < 1) {
			std::cout << "Clearing all bodies.\n";
			return;
		} else {
			std::cout << "Creating " << bcount << " bodies.\n";
		}
		if (args.size() >= 3) {
			xrange = atoi(args[2].c_str());
			if (xrange < 1)
				xrange = Constants::DEFAULT_WINDOW_XSIZE;
			if (args.size() >= 4) {
				zrange = atoi(args[3].c_str());
				if (zrange < 1)
					zrange = Constants::DEFAULT_WINDOW_RATIO*Constants::DEFAULT_WINDOW_XSIZE;
			}
		}
		for (long i=0; i < bcount; i++) {
			float m = (sin(rand())*10);
			if (m < 0) {
				m *= -1;
			} else if (m == 0) {
				m = 1;
			}
			game.create_body(m, rand()%xrange, rand()%zrange, sin(rand())/10, sin(rand())/10);
		}
	}
}

void Command::run(std::vector<std::string> args, Game& game)
{
	if (args.size() == 1) {
		std::cout << "Usage: run [amount of iterations] ['instant' to skip animation]\n";
		return ;
	}
	int c = atoi(args[1].c_str());
	if (c < 1) {
		std::cout << "Too little!\n";
		return;
	}
	
	bool animate = true;
	if (args.size() >= 3 && args[2] == "instant")
		animate = false;
	game.iterate(c, animate);
}

void Command::create(std::vector<std::string> args, Game& game)
{
	if (args.size() == 1) {
		std::cout << "Usage: create [mass] [position x] [position z] [velocity x] [velocity z] [size]\n";
		return;
	}
	int params[] = {1, 0, 0, 0, 0, -1};	// Mass, x, z, vx, vz, size; respectively. Negative (default) size will be replaced by automated size based on mass.
	for (unsigned short i=0; i < args.size()-1 && i < sizeof(params)/sizeof(int); i++) {
		params[i] = atoi(args[i+1].c_str());
	}
	game.create_body(params[0], params[1], params[2], params[3], params[4], params[5]);
}

void Command::falloff(Game& game)
{
	if (game.quadratic_falloff == false) {
		game.quadratic_falloff = true;
		std::cout << "Gravity falloff is now quadratic (default).\n";
	} else if (game.quadratic_falloff == true) {
		game.quadratic_falloff = false;
		std::cout << "Gravity falloff is now linear.\n";
	}
}

void Command::collision_mode(std::vector<std::string> args, Game& game)
{
	if (args.size() == 1) {
		std::cout << "Usage: collision_mode (ignore/merge)\nignore - bodies will ignore each other\nmerge - bodies will merge into the more massive one\n";
		return ;
	}
	if (args[1] == "ignore")
		game.set_collision_mode(CollisionMode::Ignore);
	else if (args[1] == "merge")
		game.set_collision_mode(CollisionMode::Merge);
	else
		std::cout << "Invalid collision mode name\n";
}

void Command::info(Game& game)
{
	if (game.quadratic_falloff)
		std::cout << "Gravity falloff is quadratic.\n";
	else
		std::cout << "Gravity falloff is linear.\n";
	
	const enum CollisionMode collision_mode = game.get_collision_mode();
	std::cout << "Collision mode is ";
	if (collision_mode == CollisionMode::Ignore)
		std::cout << "ignore.\n";
	else if (collision_mode == CollisionMode::Merge)
		std::cout << "merge.\n";
	else
		std::cout << "undefined\n";
	
	const size_t body_amount = game.get_body_amount();
	if (body_amount == 0) {
		std::cout << "No objects to show info about.\n";
	}
	for (unsigned long i=0; i < body_amount; i++) {
		const Body &c = game.get_body(i);
		if (!c.hidden)
			std::cout << "m:" << c.m << " x:" << c.x << " z:" << c.z << " vx:" << c.v[0] << " vz:" << c.v[1] << "\n";
	}
}

void Command::meta()
{
	std::cout << GAMEINFO;
}

void Command::help()
{
	std::cout << "Available commands:\n- init\n- run\n- create\n- falloff\n- collision_mode\n- info\n- meta\n- help\n- exit/quit\n";	// I should do something about this
}
