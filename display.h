#ifndef DISPLAY_H
#define DISPLAY_H

#include "include.h"
#include "constants.h"
#include "body.h"

class Display
{
private:
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Texture *texture;
	float scale;
	int window_size;
	const float ARATIO = Constants::DEFAULT_WINDOW_RATIO;
public:
	void render(std::vector<Body> Bodies);
	Display(int ws = Constants::DEFAULT_WINDOW_XSIZE, float s = 1);
	~Display();
};

#endif
