#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace Constants {
	const int DEFAULT_WINDOW_XSIZE = 800;
	const float DEFAULT_WINDOW_RATIO = 5.0/8;	// Ratio of Z to X
}

#endif
