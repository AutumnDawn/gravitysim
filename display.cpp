// Made by, and copyright Autumn Dawn (2021-2023); license TBD.
#include "display.h"

void Display::render(std::vector<Body> Bodies)
{
	SDL_SetRenderTarget(renderer, texture);
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
    SDL_RenderClear(renderer);
	
	for (unsigned long i=0; i < Bodies.size(); i++) {
		const Body &c = Bodies[i];
		if (!c.hidden)
			filledCircleRGBA(renderer, (c.x)*scale, (c.z)*scale, scale*int(c.size), 255, 255, 255, 255);
	}
	SDL_SetRenderTarget(renderer, NULL);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
}

Display::Display(int ws, float s)
{
	SDL_Init(SDL_INIT_VIDEO);
	int wx = s*ws;
	int wy = ARATIO*ws*s;
	scale = s;
	window_size = ws;
	window = SDL_CreateWindow("Gravity Simulation", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, wx, wy, SDL_WINDOW_OPENGL);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, wx, wy);
}

Display::~Display()
{
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}
