#ifndef GAME_H
#define GAME_H

#include "include.h"
#include "body.h"
#include "display.h"
#include "inputhandler.h"
#include "constants.h"

enum class CollisionMode
{
	Ignore,  // Bodies will ignore each other
	Merge   // Bodies will merge into the bigger body
};

class Game
{
private:
	bool running;
	int iterations; // How many iterations the game should go through on next run loop
	bool animate;   // Whether the iterations should be animated
	enum CollisionMode collision_mode;
	Display display;
	
	//extern void display();
	std::vector<Body> Bodies;
	void iterateBodies();
public:
	bool quadratic_falloff; // TODO make this private
	
	void quit();
	void clear_bodies();
	void create_body(float mass, float pos_x = 0, float pos_z = 0, float vx = 0, float vz = 0, float size = -1);
	Body& get_body(size_t i);
	size_t get_body_amount();
	void set_collision_mode(enum CollisionMode collision_mode);
	enum CollisionMode get_collision_mode();
	
	void iterate(int it, bool animate);
	Game(int window_size = Constants::DEFAULT_WINDOW_XSIZE, float scale = 1);
	int start();
};

#endif
