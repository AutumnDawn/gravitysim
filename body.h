#ifndef BODY_H
#define BODY_H
#include "include.h"

class Body
{
public:
	// Defines if the body is treated as existing
	bool hidden;
	float v[2];
	float m;
	float x;
	float z;
	float size;
	void simulateGravity(std::vector<Body> &Bodies, const bool quadratic_falloff = false);	// Simulates the effects of gravity upon the body. Affects velocity.
	void move();
	// Maybe should be using more constructors?
	Body(float mass, float pos_x = 0, float pos_z = 0, float vx = 0, float vz = 0, float size = -1);
};

#endif
