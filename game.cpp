// Made by, and copyright Autumn Dawn (2021-2023); license TBD.
#include "game.h"

int main(int argc, char **argv)
{
	int window_size = Constants::DEFAULT_WINDOW_XSIZE;
	float scale = 1;
	if (argc > 1) {
		window_size = atoi(argv[1]);
		if (window_size == -1)
			window_size = Constants::DEFAULT_WINDOW_XSIZE;
		else if (window_size < 80) {
			std::cout << "Window size too small, minimum 80.\n";
			return 1;
		}
		if (argc > 2) {
			scale = 0.01 * atoi(argv[2]);
			if (scale < 0.5) {
				std::cout << "Scale too small, minimum 50%.\n";
				return 1;
			}
		}
	}
	srand(time(0));
	Game game(window_size, scale);
	return game.start();
}

Game::Game(int window_size, float scale): display(window_size, scale)
{
	running = true;
	iterations = 0;
	animate = false;
	quadratic_falloff = true;
	collision_mode = CollisionMode::Ignore;
	
};

void Game::iterateBodies()
{
	const size_t bsize = Bodies.size();
	for (unsigned long i=0; i < bsize; i++) {
		if (!Bodies[i].hidden)
			Bodies[i].simulateGravity(Bodies, quadratic_falloff);
	}
	for (unsigned long i=0; i < bsize; i++) {
		if (!Bodies[i].hidden)
			Bodies[i].move();
	}
	
	if (collision_mode != CollisionMode::Merge)
		return;
	
	for (size_t i=0; i<bsize; i++) {
		if (Bodies[i].hidden)
			continue;
		for (size_t j=i+1; j<bsize; j++) {
			if (Bodies[j].hidden)
				continue;
			const Body &b1 = Bodies[i];
			const Body &b2 = Bodies[j];
			float dist_sq = (b1.x-b2.x)*(b1.x-b2.x) + (b1.z-b2.z)*(b1.z-b2.z);
			if (dist_sq < (b1.size+b2.size)*(b1.size+b2.size)) {
				const float msum = b1.m + b2.m;
				// Average velocity and position of the two bodies, weighted by mass
				const float avg_v[2] = {(b1.v[0]*b1.m + b2.v[0]*b2.m)/msum, (b1.v[1]*b1.m + b2.v[1]*b2.m)/msum};
				const float mid_x = (b1.x*b1.m + b2.x*b2.m)/msum;
				const float mid_z = (b1.z*b1.m + b2.z*b2.m)/msum;
				const float size = sqrt(b1.size*b1.size + b2.size*b2.size);
				Bodies[i] = Body(msum, mid_x, mid_z, avg_v[0], avg_v[1], size);
				Bodies[j].hidden = true;
			}
		}
	}
}
/*
void Game::display()
{
	std::vector<std::vector<char> > render;
	render.resize(41);
	for (unsigned long i=0; i < 41; i++) {
		for (unsigned long j=0; j < 41; j++) {
			render[i].push_back(' ');
		}
	}
	for (unsigned long i=0; i < Bodies.size(); i++) {
		Body *c = &Bodies[i];
		if (c->x > 20 || c->x <= -20 || c->z > 20 || c->z <= -20)
			continue;
		char icon;
		if (c->m <= 2) {
			icon = '.';
		} else if (c->m > 2 && c->m <= 5) {
			icon = 'o';
		} else {
			icon = 'O';
		}
		render[20 + int(c->z)][20 + int(c->x)] = icon;
	}
	std::string d_string;
	for (unsigned long i=0; i < 41; i++) {
		for (unsigned long j=0; j < 41; j++) {
			d_string.push_back(render[i][j]);
		}
		d_string.push_back('\n');
	}
	std::cout << d_string << "\n\n> ";
}
*/

void Game::clear_bodies()
{
	Bodies.clear();
}

void Game::create_body(float mass, float pos_x , float pos_z, float vx, float vz, float size)
{
	Bodies.push_back(Body(mass, pos_x, pos_z, vx, vz, size));
}

Body& Game::get_body(size_t i)
{
	return Bodies[i];
}

size_t Game::get_body_amount()
{
	return Bodies.size();
}

void Game::set_collision_mode(CollisionMode collision_mode)
{
	this->collision_mode = collision_mode;
}

enum CollisionMode Game::get_collision_mode()
{
	return collision_mode;
}

void Game::iterate(int it, bool animate)
{
	iterations = it;
	this->animate = animate;
}

int Game::start()
{
	do {
		std::vector<std::string> input;
		display.render(Bodies);
		std::cout << "> ";
		input = InputHandler::takeInput();
		InputHandler::parseInput(input, *this);
		while (iterations > 0) {
			iterateBodies();
			if (animate) {
				display.render(Bodies);
				SDL_Delay(20);
			}
			iterations--;
		}
	} while (running);
	return 0;
}

void Game::quit()
{
	running = false;
}
