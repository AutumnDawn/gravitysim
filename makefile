src = ./*.cpp
CXX = g++
CXX_DEBUG = clang++
CFLAGS = -Wall -std=c++11
LFLAGS = `sdl2-config --cflags --libs` -lSDL2_gfx
obj = ./*.o

obj: ${src}
	-${CXX} -c ${src} ${CFLAGS}

gravity: ${src}
	${CXX} ${src} ${CFLAGS} -s -o gravity ${LFLAGS}

from_obj: ${obj}
	${CXX} ${obj} ${CFLAGS} -s -o gravity ${LFLAGS}

debug: ${src}
	${CXX_DEBUG} ${src} ${CFLAGS} -g -o debug ${LFLAGS}
