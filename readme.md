A simple gravity simulator.

Usage:
======
`gravity [horizontal window size (-1 for default=800)] [scale in %]`. The arguments are optional.

`help` in-game for informations about particular commands.

Compilation:
============
Dependencies:
------------
- SDL2, SDL2_gfx
- g++ or clang++ (though probably any compiler would work)

Build:
------
- `make gravity` to build the default executable (needs g++ by default)
- `make debug` to build debug executable (needs clang++ by default)
- Modify the makefile if you want to use a different compiler since I'm lazy.
