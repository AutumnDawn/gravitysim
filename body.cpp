// Made by, and copyright Autumn Dawn (2021-2023); license TBD.
#include "body.h"

const float GConst = 0.05;

void Body::simulateGravity(std::vector<Body> &Bodies, const bool quadratic_falloff)
{
	float f_vec[2] = {0, 0};
	for (unsigned long i=0; i < Bodies.size(); i++) {
		const Body &c = Bodies[i];
		if (&Bodies[i] == this || c.hidden)
			continue;
		float dist_sq = ((c.x)-x)*((c.x)-x) + ((c.z)-z)*((c.z)-z);	// Square of the Euclidean distance
		float fx = 0;
		float fz = 0;
		if (dist_sq != 0) {
			fx = ((c.x) - x)*m*(c.m)*GConst / dist_sq;				// Calculate gravity vector
			fz = ((c.z) - z)*m*(c.m)*GConst / dist_sq;
		}
		
		/* Divide yet again by distance to reduce it to quadratic falloff, as in reality.
		 * The default behavior has linear falloff, as the way direction vector is deduced
		 * is based on multiplication/addition of the distance:
		 * (d*r)/(d^2) = r/d; where d=distance, r=the remaining part of equation.
		 * ( This part changes it to (d*r)/(d^3) = r/(d^2) )
		 */
		if (quadratic_falloff && dist_sq != 0) {
			float dist = sqrt(dist_sq);
			fx /= dist;
			fz /= dist;
		}
		f_vec[0] += fx;
		f_vec[1] += fz;
	}
	if (m != 0) {
		v[0] += f_vec[0]/m;
		v[1] += f_vec[1]/m;
	}
}

void Body::move()
{
	x += v[0];
	z += v[1];
}

Body::Body(float mass, float pos_x, float pos_z, float vx, float vz, float size)
{
	hidden = false;
	m = mass;
	x = pos_x;
	z = pos_z;
	v[0] = vx;
	v[1] = vz;
	if (size > 0)
		this->size = size;
	else
		this->size = sqrt((mass) * 2);
}
